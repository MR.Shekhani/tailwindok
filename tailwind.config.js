/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      backgroundImage:{
        'hero-pattern': "url('./images/img1.svg')",
      }, 
      
      colors:{
        abc : "rgba(62, 159, 227, 1)"
      },
      size:{
        68 : "265px"
      }
    },
    boxShadow: {
      '3xl': '0px 42px 34px rgba(82, 67, 194, 0.295755)',
    },
    rounded:{
      'rounded-md': '4px',
    },
    
  },
  plugins: [],
}
